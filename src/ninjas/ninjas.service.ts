import { Injectable } from '@nestjs/common';
import { CreateNinjaDto } from './dto/create-ninja.dto';
import { UpdateNinjaDto } from './dto/update-ninja.dto';

@Injectable()
export class NinjasService {
  private ninjas = [
    { id: 0, name: 'ninjaA', weapon: 'stars' },
    { id: 1, name: 'ninjaB', weapon: 'nunchucks' },
  ];

  // get all ninja
  getNinjas(weapon: 'stars' | 'nunchucks') {
    if (weapon) {
      this.ninjas.filter((ninja) => ninja.weapon === weapon);
    }

    return this.ninjas;
  }

  // get one ninja
  getOneNinja(id: number) {
    const ninja = this.ninjas.find((ninja) => ninja.id == id);

    if (!ninja) {
      throw new Error('ninja not found');
    }

    return ninja;
  }

  // create ninja
  createNinja(createNinjaDto: CreateNinjaDto) {
    const newNinja = {
      ...createNinjaDto,
      id: Date.now(),
    };

    this.ninjas = [...this.ninjas, newNinja];

    return newNinja;
  }

  // update ninja
  updateNinja(id: number, updateNinjaDto: UpdateNinjaDto) {
    this.ninjas = this.ninjas.map((ninja) => {
      if (ninja.id === id) {
        return { ...ninja, ...updateNinjaDto };
      }

      return ninja;
    });

    return this.getOneNinja(id);
  }

  removeNinja(id: number) {
    const toBeRemoved = this.getOneNinja(id);

    this.ninjas = this.ninjas.filter((ninja) => ninja.id != id);

    return toBeRemoved;
  }
}
